kind: manual_second


depends:
- dependency: gcc-cross
  scope: Build
- dependency: make
  scope: Build
- dependency: kmod
  scope: Build

sources:
  - kind: git
    url: https://github.com/raspberrypi/firmware
    sha: 4bb9d889a9d48c7335ebe53c0b8be83285ea54b1

context: build_target

provides: firmware

plugin_variables:
  join_strategy: Join
  variables:

    config_install-commands: |
      mkdir -p %{install-root}/boot %{install-root}/etc


      rm boot/*.img
      rm boot/*.dtb
      rm boot/overlays/*.dtbo
      cp -r boot/* %{install-root}/boot


      cat >%{install-root}/boot/config.txt <<EOF
      # For more options and information see
      # http://rptl.io/configtxt
      # Some settings may impact device functionality. See link above for details

      # Uncomment some or all of these to enable the optional hardware interfaces
      #dtparam=i2c_arm=on
      #dtparam=i2s=on
      #dtparam=spi=on

      # Enable audio (loads snd_bcm2835)
      dtparam=audio=on

      # Additional overlays and parameters are documented
      # /boot/firmware/overlays/README

      # Automatically load overlays for detected cameras
      camera_auto_detect=1

      # Automatically load overlays for detected DSI displays
      display_auto_detect=1

      # Automatically load initramfs files, if found
      auto_initramfs=1

      # Enable DRM VC4 V3D driver
      dtoverlay=vc4-kms-v3d
      max_framebuffers=2

      # Don't have the firmware create an initial video= setting in cmdline.txt.
      # Use the kernel's default instead.
      #disable_fw_kms_setup=1

      # Run in 64-bit mode
      arm_64bit=1

      # Disable compensation for displays with overscan
      disable_overscan=1

      # Run as fast as firmware / board allows
      arm_boost=1

      [cm4]
      # Enable host mode on the 2711 built-in XHCI USB controller.
      # This line should be removed if the legacy DWC2 controller is required
      # (e.g. for USB device mode) or if USB support is not required.
      otg_mode=1

      [cm5]
      dtoverlay=dwc2,dr_mode=host

      [all]
      EOF

      cat >%{install-root}/boot/cmdline.txt <<EOF
      console=tty1 console=serial0,115200 root=/dev/mmcblk0p2 rw rootfstype=ext4 rootwait
      EOF

      cat >%{install-root}/etc/fstab <<EOF
      /dev/mmcblk0p1 /boot/ vfat dmask=000,fmask=0111,user 0 0
      /dev/mmcblk0p2 / ext4 defaults 0 1
      EOF
