kind: manual_second

provides: kernel-target

depends:
- dependency: gcc-cross
  scope: Build
- dependency: make
  scope: Build
- dependency: kmod
  scope: Build
- dependency: flex
  scope: Build
- dependency: m4
  scope: Build
- dependency: bison-pass3
  scope: Build
- dependency: bc
  scope: Build
- dependency: gzip-pass1
  scope: Build
- dependency: openssl
  scope: Build
- dependency: elfutils
  scope: Build
- dependency: perl-pass3
  scope: Build
- dependency: diffutils-pass1
  scope: Build

sources:
  - kind: tar
    url: https://www.kernel.org/pub/linux/kernel/v6.x/linux-6.4.12.tar.xz
    unpack: linux-6.4.12
    sha: cca91be956fe081f8f6da72034cded96fe35a50be4bfb7e103e354aa2159a674

context: build_target

plugin_variables:
  join_strategy: Join
  variables:
    host-arch: x86
    make: make -j10 O=build %{make_arch}  %{make_compiler} $TARGET modules
    config_build-commands: "%{make}"
    
    config_configure-commands: |
      echo "root:x:0:0:root:/root:/bin/bash" > /etc/passwd
      make O=build %{make_arch} %{make_compiler} defconfig

      cd build
      touch config_extra.fragments
      #echo "CONFIG_MODULE_COMPRESS_NONE=y" >> config_extra.fragments
      echo "CONFIG_MODULE_COMPRESS_GZIP=y" >> config_extra.fragments


      #scripts/config -e DEVTMPFS
      #scripts/config -e CGROUPS
      #scripts/config -e INOTIFY_USER
      #scripts/config -e SIGNALFD
      #scripts/config -e TIMERFD
      #scripts/config -e EPOLL
      #scripts/config -e NET
      #scripts/config -e SYSFS
      #scripts/config -e PROC_FS
      #scripts/config -e FHANDLE

      #scripts/config -e CRYPTO_USER_API_HASH
      #scripts/config -e CRYPTO_HMAC
      #scripts/config -e CRYPTO_SHA256

      #scripts/config -d SYSFS_DEPRECATED

      # Kernel Config Options
      echo "CONFIG_DEVTMPFS=y" >> config_extra.fragments
      echo "CONFIG_CGROUPS=y" >> config_extra.fragments
      echo "CONFIG_INOTIFY_USER=y" >> config_extra.fragments
      echo "CONFIG_SIGNALFD=y" >> config_extra.fragments
      echo "CONFIG_TIMERFD=y" >> config_extra.fragments
      echo "CONFIG_EPOLL=y" >> config_extra.fragments
      echo "CONFIG_NET=y" >> config_extra.fragments
      echo "CONFIG_SYSFS=y" >> config_extra.fragments
      echo "CONFIG_PROC_FS=y" >> config_extra.fragments
      echo "CONFIG_FHANDLE=y" >> config_extra.fragments

      # Kernel crypto/hash API
      echo "CONFIG_CRYPTO_USER_API_HASH=y" >> config_extra.fragments
      echo "CONFIG_CRYPTO_HMAC=y" >> config_extra.fragments
      echo "CONFIG_CRYPTO_SHA256=y" >> config_extra.fragments

      # udev will fail to work with legacy sysfs
      echo "CONFIG_SYSFS_DEPRECATED=n" >> config_extra.fragments

      # Some udev/virtualization requires
      echo "CONFIG_DMIID=y" >> config_extra.fragments
      echo "CONFIG_DMI_SYSFS=y" >> config_extra.fragments
      echo "CONFIG_FW_CFG_SYSFS=y" >> config_extra.fragments
      echo "CONFIG_VIRTIO_INPUT=y" >> config_extra.fragments


      if [ "%{target-gcc-triplet}" == 'aarch64-lfs-linux-gnu' ]; then
        echo "CONFIG_FONT_SUPPORT=y" >> config_extra.fragments
        echo "CONFIG_FONT_8x16=y" >> config_extra.fragments
        echo "CONFIG_MEDIA_CONTROLLER=y" >> config_extra.fragments
        echo "CONFIG_VIDEO_V4L2_SUBDEV_API=y" >> config_extra.fragments
        echo "CONFIG_VIDEOBUF2_VMALLOC=m" >> config_extra.fragments
        echo "CONFIG_VIDEOBUF2_DMA_CONTIG=m" >> config_extra.fragments
        echo "CONFIG_VIDEO_V4L2_TPG=n" >> config_extra.fragments
        #echo "CONFIG_VIMC=y" >> config_extra.fragments
        echo "CONFIG_MEDIA_TEST_SUPPORT=y" >> config_extra.fragments
        echo "CONFIG_V4L_TEST_DRIVERS=y" >> config_extra.fragments

        echo "CONFIG_VIDEO_VIM2M=m" >> config_extra.fragments
        echo "CONFIG_VIDEO_VIMC=m" >> config_extra.fragments
        echo "CONFIG_VIDEO_VIVID=m" >> config_extra.fragments

        echo "CONFIG_PCI=y" >> config_extra.fragments
        echo "CONFIG_VIRTIO_PCI=y" >> config_extra.fragments
        echo "CONFIG_PCI_HOST_GENERIC=y" >> config_extra.fragments
        echo "CONFIG_DRM_VIRTIO_GPU=y" >> config_extra.fragments

        TARGET=Image
      else
        #echo "CONFIG_UNWINDER_ORC=n" >> config_extra.fragments
        TARGET=bzImage
      fi

      echo "CONFIG_UEVENT_HELPER=n" >> config_extra.fragments
      ../scripts/kconfig/merge_config.sh -m .config config_extra.fragments
      cd ../

    config_install-commands: |
      make INSTALL_MOD_PATH="/%{install-root}/usr" O=build %{make_arch} %{make_compiler} modules_install
      mkdir -p /%{install-root}/boot/
      if [ "%{target-gcc-triplet}" == 'aarch64-lfs-linux-gnu' ]; then
        cp -rL build/arch/arm64/boot/*Image /%{install-root}/boot/
        #make -j1 INSTALL_DTBS_PATH='%{install-root}/boot/dtbs' dtbs_install
      else
        cp -rL build/arch/x86_64/boot/*Image /%{install-root}/boot/
      fi
      rm -rf /%{install-root}/boot/dts
