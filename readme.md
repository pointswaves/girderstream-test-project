# Heather linux

This project uses [grdtools](https://gitlab.com/gorse/grdtools) as its tool chain and produces a linux system.

Heather linux aims is small and hardy like the plant.

It is expected to be run in:
* Embedded systems
* Small docker images as you might use alpine linux

Currently its only supported build arch is `x86_64` but this might be expanded.

Currently it targets `x86_64` and `aarch64` with most work targeting `aarch64`. It is expected to support
more target systems as the community grows.

Currently libdrm is build but only very very basic graphics are currently built, this is expected to
to expand in the future.

## Options


### BSP

Currently you can pick; `pi` or `qemu`

The `pi` bsp is tested on pi-zero-2 and on pi 4 with support for pi5 in development.

The `qemu` bsp is tested for aarch64 and x86_64

### User land

A basic userland is provided for both busybox and gnu

The `busybox` userland is very basic but working.

The `GNU` userland can be use with sytemd and udev and works with quite a lot of hardware including some cameras and some netwroking

## Selecting target

We use a girderstream option to pick the target ie

```bash
girderstream --options "bsp=pi"  --verbose build --name busybox
```

Or

```bash
girderstream --options "bsp=qemu"  --verbose build --name busybox
```


## Local cas

As per the girderstream instructions, currently you need a local cas running and you must do this your self

For example

```bash
buildbox-casd  --bind localhost:50040 ~/.cache/girderstream/cas
```

## Using the project

You can check the state of a build with

```bash
girderstream --verbose show --name kernel
```

You can build a element with

```bash
girderstream --options bsp=qemu build --name filesystem-target
```

And checkout said element with

```bash
girderstream --options bsp=qemu --options arch=aarch64 checkout --name filesystem-target --location ./filesystem_aarch64
```

## Girderstream Compatiablity

Girderstream is in active development, so it is best to use the latest version of the main branch of girdersteam with the
latest version of the main branch with this project.


## Running Qemu targets

To run the x86_64 version with qemu:

First build it

```bash
girderstream --options bsp=qemu --options arch=x86_64 build --name filesystem-target
```

Check out the required parts

```bash
girderstream --options bsp=qemu --options arch=x86_64 checkout --name kernel-target --location ./linux_x86
girderstream --options bsp=qemu --options arch=x86_64 checkout --name filesystem-target --location ./filesystem_x86
```

And then run qemu in text or graphic mode

```bash
qemu-system-x86_64 -nographic -drive file=filesystem_x86/disk.img,format=raw -kernel linux_x86/boot/bzImage -append "earlyprintk=serial,ttyS0 console=ttyS0 root=/dev/sda2 rw"

qemu-system-x86_64 -drive file=filesystem_x86/disk.img,format=raw -kernel linux_x86/boot/bzImage -append "earlyprintk=serial,ttyS0 console=ttyS0 root=/dev/sda2 rw"
```

```bash
/ # /usr/local/bin/hello
This is amhello 1.0.
```

The following will boot the aarch64 image:

```bash
girderstream --options bsp=qemu --options arch=aarch64 build --name filesystem-target
 
girderstream --options bsp=qemu --options arch=aarch64 checkout --name kernel-target --location ./linux_aarch64
girderstream --options bsp=qemu --options arch=aarch64 checkout --name filesystem-target --location ./filesystem_aarch64

qemu-system-aarch64 -device virtio-gpu-pci -machine virt -cpu cortex-a57 -machine type=virt -nographic -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw"
```

#### Systemd

The following will boot the aarch64 image:

```bash
girderstream --options bsp=qemu --options init-system=systemd --options arch=aarch64 build --name filesystem-target
 
girderstream --options bsp=qemu --options init-system=systemd --options arch=aarch64 checkout --name kernel-target --location ./linux_aarch64
girderstream --options bsp=qemu --options init-system=systemd --options arch=aarch64 checkout --name filesystem-target --location ./filesystem_aarch64

qemu-system-aarch64 -device virtio-gpu-pci -machine virt -cpu cortex-a57 -machine type=virt -nic user,model=virtio,hostfwd=tcp::2222-:22 -nographic -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw"
ssh root@localhost -p 2222
```

### Debuging qemu target

you can check the disk

```bash
fdisk -lu filesystem_aarch64/disk.img
Disk filesystem_aarch64/disk.img: 1.27 GiB, 1364197376 bytes, 2664448 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device                       Boot  Start     End Sectors  Size Id Type
filesystem_aarch64/disk.img1 *      2048  616447  614400  300M  c W95 FAT32 (LBA)
filesystem_aarch64/disk.img2      616448 2664447 2048000 1000M 83 Linux
```

and mount it

```bash
offset=Start*512

sudo mkdir /mnt/disk.img.partition
sudo mount -o loop,offset=315621376 filesystem_aarch64/disk.img /mnt/disk.img.partition
sudo mount -o loop filesystem_aarch64/root.img /mnt/disk.img.partition

ls /mnt/disk.img.partition/

sudo umount /mnt/disk.img.partition/
sudo rmdir /mnt/disk.img.partition/
```

``` bash
qemu-system-aarch64 -device virtio-gpu-pci  -machine virt -cpu cortex-a57 -machine type=virt -nographic -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw init=/usr/bin/bash"
bash-5.2# journalctl -u systemd-logind.service
bash-5.2# systemctl status systemd-logind.service
```


To boot with a video output setup you can use:

```bash
qemu-system-aarch64 -machine virt -cpu cortex-a57 -machine type=virt -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw init=/usr/lib/systemd/systemd" \
-object rng-random,id=rng0,filename=/dev/urandom \
-device virtio-rng-pci,rng=rng0 \
-device virtio-vga-gl \
-display gtk,gl=on \
-device virtio-keyboard-pci \
-device virtio-tablet-pci \
-vga virtio


-device virtio-tablet \
-device virtio-keyboard \
-device qemu-xhci,id=xhci \
-device virtio-vga-gl \
-display gtk,gl=on \
-object input-linux,id=kbd1,evdev=/dev/input/event7,grab_all=on,repeat=on

\
-object input-linux,id=mouse1,evdev=/dev/input/by-id/usb-046a_USB_Device-mouse \
-object input-linux,id=kbd1,evdev=/dev/input/by-id/usb-Massdrop_Inc._CTRL_Keyboard_1642588152-event-kbd,grab_all=on,repeat=on

-device virtio-gpu-pci
```





-display gtk,gl=on
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::4444-:5555

Inside the running qemu you can then run

```bash
/ # modprobe virtio_gpu
/ # modprobe vivid
/ # modprobe drm
/ # modprobe i2c-dev
```

```bash
i2cdetect -l
i2cdetect -y 1
```

And then you will see the virtio screen turn on and have some text from the kernel.

todo https://docs.kernel.org/admin-guide/media/vimc.html

```bash
/ # cam --camera 1 -C -D
[0:13:53.742593680] [267]  INFO Camera camera_manager.cpp:299 libcamera v0.0.4
[0:13:53.857987264] [268]  WARN CameraSensorProperties camera_sensor_properties.cpp:231 No static properties available for 'Sensor B'
[0:13:53.859118128] [268]  WARN CameraSensorProperties camera_sensor_properties.cpp:233 Please consider updating the camera sensor properties database
[0:13:53.859943872] [268]  WARN CameraSensor camera_sensor.cpp:459 'Sensor B': Failed to retrieve the camera location
Using camera platform/vimc.0 Sensor B as cam0
[0:13:53.924722128] [267]  INFO Camera camera.cpp:1028 configuring streams: (0) 1920x1080-BGR888
Unable to find display pipeline for format BGR888
Failed to configure frame sink
Failed to start camera session
```

cam --camera 1 --capture=5 -D

```bash
SYSTEMD_LOG_LEVEL=debug udevadm test /sys/class/input/event0 
```

## Using weston and qemu

To build the qemu images with working weston:

```bash
girderstream --options bsp=qemu --options init-system=systemd --options arch=aarch64 --options windowing=wayland build --name filesystem-target
 
girderstream --options bsp=qemu --options init-system=systemd --options arch=aarch64 --options windowing=wayland checkout --name kernel-target --location ./linux_aarch64
girderstream --options bsp=qemu --options init-system=systemd --options arch=aarch64 --options windowing=wayland checkout --name filesystem-target --location ./filesystem_aarch64
```

To use weston in qemu:
```bash
qemu-system-aarch64 -device virtio-gpu-gl -machine virt -cpu cortex-a57 -machine type=virt -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw" -display gtk,gl=on -device virtio-keyboard-pci -device virtio-tablet-pci
export XDG_RUNTIME_DIR=/run/user/
seatd -g video &
weston
```

## Using the pi image

First build and checkout the PI with

```bash
girderstream --options bsp=pi --options init-system=systemd --options windowing=wayland --options camera=include build --name filesystem-target
girderstream --options bsp=pi --options init-system=systemd --options windowing=wayland --options camera=include checkout --name filesystem-target --location ./filesystem_pi
```

For the pi you should write to a sd card like:

```bash
sudo dd bs=64k status=progress of=/dev/sdX if=filesystem_pi/disk.img
```

Changing sdX to point to your SD card and taking care not to point to
anything important.

And then you can use the sd card to boot the pi.

Once you boot the pi you can then run 

```bash
cam --camera 1 --capture=5 -C -D
```

And the screen will be filled with a camera preview

### Pi networking

Heather linux comes with tools to support networking

When using a systemd userland the system should tread any wired network as a dhcp client and automaticly connect

Wire less networks should be connected to with iw, currently:

```bash
ip link set wlan0 up
iw dev wlan0 connect -w ssid
```

When tested on the pi zero 2 results in a kernel crash.

On a pi4 it seem to work but dhcp is not triggered

```bash
ip dev wlan0 link
```

Gives details

```bash
ip addr add 10.42.0.50 dev wlan0
ip route add 10.42.0.0/24 dev wlan01
```

Will then allow

```bash
ssh will@10.42.0.1
```

## Using busybox userland

This can be done for the pi also but the following is a example for aarch64 qemu


```bash
girderstream --options bsp=qemu --options user-land=busybox --options arch=aarch64 build --name filesystem-target
 
girderstream --options bsp=qemu --options user-land=busybox --options arch=aarch64 checkout --name kernel-target --location ./linux_aarch64
girderstream --options bsp=qemu --options user-land=busybox --options arch=aarch64 checkout --name filesystem-target --location ./filesystem_aarch64

qemu-system-aarch64 -device virtio-gpu-pci -machine virt -cpu cortex-a57 -machine type=virt -nographic -smp 2 -m 4096 -kernel linux_aarch64/boot/Image -drive file=filesystem_aarch64/disk.img,format=raw -append "root=/dev/vda2 rw"
```

## SDK

To build a SDK for local app development you can build the system-sdk element and check it out.

```bash
girderstream --options windowing=wayland --options camera=include build --name system-sdk
rm -rf system-sdk
girderstream --options windowing=wayland --options camera=include checkout --name system-sdk --location ./system-sdk
```

All the required tools for local app development will now be in `system-sdk` these include the host tools and
the target libraries. Note that the system tools have not been build with the grdtools host libraries so you
will probably need chroot or podman/docker to use them properly.

You can load the sdk ca into docker/podman with the provided docker file from this repo. eg:

```bash
podman build --tag sdk-aarch64:local .
```

Once you have the docker image built you can start a docker shell in your sdk as follows:

```bash
podman run -it -v `pwd`:/src/:z --workdir /src/ sdk-aarch64:local bash
```

This project also publishes the sdk as a docker image to its gitlab registry and they can be pulled and run as follows

```bash
podman pull registry.gitlab.com/pointswaves/girderstream-test-project/sdk-aarch64:latest
podman run -it -v `pwd`:/src/:z --workdir /src/ registry.gitlab.com/pointswaves/girderstream-test-project/sdk-aarch64:latest bash
```



