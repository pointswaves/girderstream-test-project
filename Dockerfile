# syntax=docker/dockerfile:1
FROM scratch
ADD system-sdk /
ENV PKG_CONFIG_PATH /opt/cross/usr/lib/pkgconfig/
ENV PKG_CONFIG_LIBDIR /opt/cross/usr/lib/pkgconfig/
ENV PKG_CONFIG_SYSROOT_DIR /opt/cross
ENV PATH /opt/usr/bin:/usr/bin/:/bin
ENV TARGET_CXX "aarch64-lfs-linux-gnu-g++"
ENV BINDGEN_EXTRA_CLANG_ARGS "--sysroot=/opt/cross/usr/"
